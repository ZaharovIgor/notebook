# generating phonebook
# by file with names
# and surnames and
# random phone numbers

from random import choice, randint
import sys


def get_names(file_name):
    """read file and return list of text info"""
    with open(file_name, 'r') as names_file:
        lines = names_file.readlines()

    names = []
    for line in lines:
        names.append(line.split()[0])
        del line

    return names


def get_phones(left, right):
    """return rand numbers list with unique el-s"""
    phones = []

    for i in range(6000):
        phone = randint(left, right)
        if phone not in phones:
            phones.append(phone)

    return phones


def get_phonebook(surnames, names, phones, size):
    """generate phonebook"""
    phonebook = {}
    for i in range(size):
        surname = choice(surnames)
        name = choice(names)
        phone = choice(phones)
        phonebook[surname.title() + ' ' + name.title()] = phone

    return phonebook


def main():
    """reading and updating files"""
    gen_num = int(sys.argv[1]) if len(sys.argv) > 1 else 20000
    phonebook = get_phonebook(get_names('surnames.txt'), get_names('names.txt'),
                              get_phones(99999, 1000000), size=gen_num)

    with open('phonebook.txt', 'w') as file:
        for man in sorted(phonebook.keys()):
            file.write(man + ',')


if __name__ == '__main__':
    main()
