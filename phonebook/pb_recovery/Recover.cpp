#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>


using namespace std;
#define NAMES "names.txt"


void get_names_from(string filename, back_insert_iterator<vector<string>> names)
{
	ifstream name_file(filename);

	if (!name_file.is_open())
		throw "Error! File with contact names is not found!\n";

	while (!name_file.eof()) {
		string name;
		getline(name_file, name, ',');

		if (!name.empty()) {
			names = name;
			names++;
		}
	}
	name_file.close();
}


void create_file(std::string filename) {
	std::string cmd = " rem:>";
	cmd += ('"' + filename + '"');
	system(cmd.c_str());
}


int main()
{
	ifstream names_file(NAMES);
	if (!names_file.is_open()) {
		create_file(NAMES);
		cout << "file with names created\n";
		system("pause");
		return 0;
	}
	names_file.close();

	vector<string> names;
	back_insert_iterator<vector<string>> back_it_names(names);
	get_names_from(NAMES, back_it_names);

	for (auto it = names.begin(); it != names.end(); it++) {
		string file_name = (*it) + ".txt";
		ifstream file(file_name);

		if (!file.is_open()) {
			create_file(file_name);
			cout << file_name << " is created\n";
		}
		file.close();
	}

	system("pause");
	return 0;
}