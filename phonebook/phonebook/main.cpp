#include "WinClass.h"


using namespace std;


enum {WIDTH = 25};


void get_names_from(string filename, back_insert_iterator<vector<string>> names)
{
	ifstream name_file(filename);

	if (!name_file.is_open())
		throw "Error! File with contact names is not found!\n";

	while (!name_file.eof()) {
		string name;
		getline(name_file, name, ',');

		if (!name.empty()) {
			names = name;
			names++;
		}
	}
	name_file.close();
}


string input_word(char break_sign, char input_end) {
	string word;

	char sign = '0';
	while ((int)sign != break_sign) {
		sign = getch();

		if (sign != input_end && sign != break_sign)
			//((sign >= '1' && sign <='9') || (sign >='a' && sign <='z') ||
			//(sign >='A' && sign <='Z') || sign == '@' || sign == ' ')) 
		{
			std::cout.put(sign);
			word += sign;
		}
		else
			if (sign == break_sign) {
				string empty_word;
				word.swap(empty_word);
				break;
			}
			else
				break;
	}

	return word;
}


int main()
{
	try
	{
		vector<string> names;

		string filename = "names.txt";
		back_insert_iterator<vector<string>> back_it_names(names);
		get_names_from(filename, back_it_names);

		Window win(names, WIDTH);
		win.print_main_header();
		win.print_names();
		win.set_pos(0, win.head_width());

		int x = 0, y = win.head_width(),
			line = 0; // index of name pointed to by cursor

		char action = '0';
		while ((int)action != ESC) 
		{
			action = getch();

			switch ((int)action)
			{
			case UP: {
				if (y > win.head_width()) {
					y--;
				}
				else {
					win.scroll_up();
					win.print_names();
				}

				if (line > 0) {
					line--;
				}
			} break;

			case DOWN: {
				if (y != WIDTH - 2 && line < win.names_vec_size() - 1) {
					y++;
					line++;
				}
				else {
					//win.scroll_down();
					//win.print_names();
					if (line < win.names_vec_size() - 1) {
						win.scroll_down();
						win.print_names();
						line++;
					}
				}
			} break;

			case ENTER: { // open contact info
				win.clear_scr();
				win.open_contact(line);
				x = 0; y = win.head_width();
				line = win.printing_pos_inx();
				// names printed after actions with contact info
				win.print_main_header();
				win.print_names();
			} break;

			case TAB: { // new contact adding
				win.clear_scr();

				string new_name;
				cout << "Adding new contact\ninput name: ";
				getline(cin, new_name);

				string content;
				cout << "input contact info: ";
				getline(cin, content);

				win.add_contact(new_name, content);
				// names will be printed
				// from inserted contact
				line = win.printing_pos_inx();
				x = 0; y = win.head_width();
				win.print_main_header();
				win.print_names();
			} break;

			case ONE: { // find contact in vector
				win.clear_scr();
				cout << "ESC-back ENTER-input\ninput substr to find: ";
				string word_to_find = input_word(ESC, ENTER);

				if (!word_to_find.empty()) {
					win.find_contact(word_to_find);
				}
				line = win.printing_pos_inx();
				x = 0; y = win.head_width();
				win.print_main_header();
				win.print_names();
			} break;

			case EIGHT: {
				win.clear_scr();
				cout << "ENTER-delete ESC-back\n";
				char act = '0';
				while ((int)act != ESC) {
					act = getch();
					if ((int)act == ENTER) {
						win.delete_contact(line);
						break;
					}
				}
				
				line = win.printing_pos_inx();
				x = 0; y = win.head_width();
				win.print_main_header();
				win.print_names();
			} break;
			}
			win.set_pos(x, y);
		}

		win.exit(filename);
	}
	catch (char* error) {
		cout << error;
	}
	
	system("pause");
	return 0;
}