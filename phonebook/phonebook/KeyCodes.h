#pragma once

enum { // key codes
	LEFT = 75, RIGHT = 77, UP = 72, DOWN = 80,
	TAB = 9, ENTER = 13, ESC = 27, ONE = 49,
	ZERO = 56, EIGHT = 56, PG_UP = 73, PG_DN = 81,
};