#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <deque>
#include <iterator>
#include <algorithm>
#include <iomanip>
#include <conio.h>
#include <Windows.h>

#include "KeyCodes.h"

#pragma once




class Window
{
private: // privat members

	struct FirstLetterInterval {
		// safe intervals of first letters
		char sign;
		int left_bound;
		int rigth_bound;

		FirstLetterInterval(char sign, int left, int rigth) {
			this->sign = sign;
			this->left_bound = left;
			this->rigth_bound = rigth;
		}

		bool operator==(char comp) {
			return sign == comp;
		}
	};

	std::vector<std::string> names;
	std::deque<FirstLetterInterval> intervals;
	// to formatting output
	int console_width;
	const int header_heigth = 2;
	std::vector<std::string>::iterator print_pos;
	std::string main_header = "1-FIND 8-DELETE Tab-ADD Enter-OPEN Esc-EXIT\n";
	std::string open_name_header = ": Esc-BACK 1-RENAME Enter-APPEND Tab-REWRITE\n";


private: // private methods

	COORD set_cord(int x, int y)
	{
		COORD coord;
		coord.X = x;
		coord.Y = y;

		return coord;
	}

	// methods for fast searching in names vector
	void findFirstLetterIntervals() {
		//searching intervals for every letter or sign
		intervals.clear();

		for (int i = 0; i < names.size(); i++) {
			char bufChar = names[i][0];

			int last_i = i;
			for (i; i < names.size() - 1; i++) {
				if (names[i][0] != names[i + 1][0]) {
					break;
				}
			}
			FirstLetterInterval bufCord(bufChar, last_i, i);
			intervals.push_back(bufCord);
		}
	}

	std::vector<int> get_word_range_inLetterIntervals(std::string word)
	{// binary search in intervals deque
		int left_idx = 0, rigth_idx = intervals.size() - 1;

		int averege_idx = 0;
		while (left_idx < rigth_idx) {
			averege_idx = left_idx + (rigth_idx - left_idx) / 2;

			if (word[0] <= intervals[averege_idx].sign) {
				rigth_idx = averege_idx;
			}
			else {
				left_idx = averege_idx + 1;
			}
		}
		if (word[0] == intervals[rigth_idx].sign) {
			std::vector<int> interval(2);
			interval[0] = intervals[rigth_idx].left_bound;
			interval[1] = intervals[rigth_idx].rigth_bound;
			return interval;
		}
		else {
			std::vector<int> empty_interval;
			return empty_interval;
		}
	}

	// methods used to process files
	int file_size(std::string filename) {
		// shift file ptr to eof end return number of bytes
		// passed by file ptr
		std::ifstream file(filename, std::ios_base::binary);
		if (!file.is_open())
			throw "Error! File is not found!\n";
		file.seekg(0, std::ios::end);
		int f_size = file.tellg();
		file.close();
		return f_size;
	}

	char* file_content(std::string filename, int f_size) {
		// return c_string with file content
		char* content = new char[f_size + 1];

		std::ifstream file(filename, std::ios_base::binary);
		if (!file.is_open())
			throw "Error! File is not found!\n";
		file.read(content, f_size);
		file.close();
		content[f_size] = '\0';
		return content;
	}

	void print_file(std::string filename) {
		// read file and print content on console
		int f_size = file_size(filename);
		char* content = file_content(filename, f_size);

		std::cout << content << '\n';
		delete[] content;
	}

	void delete_file(std::string filename) {
		std::string cmd = " del "; 
		cmd += ('"' + filename + '"');
		system(cmd.c_str());
	}

	void create_file(std::string filename) {
		std::string cmd = " rem:>";
		cmd += ('"' + filename + '"');
		system(cmd.c_str());
	}

	void write_file(std::string filename, std::string str, bool app_mode = false) {
		// append str to file if app_mode=True else rewrite
		std::ofstream file;
		if (app_mode) {
			file.open(filename, std::ios_base::app);
		}
		else {
			file.open(filename, std::ios_base::trunc);
		}
		if (!file.is_open())
			throw "Error! File is not found!\n";
		file << str << '\n';
		file.close();
	}

	void print_header(std::string header) {
		system("cls");
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), set_cord(0, 0));
		std::cout << header;
		for (int i = 0; i < 80; i++)
			std::cout << (char)176;
	}


public: // public methods

	Window(std::vector<std::string> &names, int heigth) {
		this->names = names;
		console_width = heigth;
		print_pos = this->names.begin();
		findFirstLetterIntervals();
	}

	void set_pos(int x, int y) {
		COORD coord;
		coord.X = x;
		coord.Y = y;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	}

	void open_contact(int line) {
		print_header(open_name_header);

		std::string filename = names.at(line) + ".txt";
		print_file(filename);

		char action = '0';
		bool act_is_done = false;
		while (!act_is_done) {
			action = getch();

			switch ((int)action)
			{
			case ENTER: {
				std::cout << "\nAPPENDING\ninput info to append: \n";
				std::string app_str;
				std::getline(std::cin, app_str);

				write_file(filename, app_str, true);
				print_file(filename);

				print_pos = names.begin() + line;
				act_is_done = true;
			} break;
			case TAB: {
				std::cout << "\nREWRITING\ninput new info: \n";
				std::string new_str;
				std::getline(std::cin, new_str);

				write_file(filename, new_str);
				print_file(filename);

				print_pos = names.begin() + line;
				act_is_done = true;
			} break;

			case ONE: {
				std::cout << "\nRENAME\ninput new contact name: ";
				std::string new_name;
				std::getline(std::cin, new_name);

				if (std::count(names.begin(), names.end(), new_name) == 0) {
					std::string prev_name = names.at(line);
					names.erase(names.begin() + line);
					
					std::string contact_info;
					char* content = file_content(filename, file_size(filename));
					contact_info = content;
					delete[] content;
					
					add_contact(new_name, contact_info);
					findFirstLetterIntervals();

					std::string filename = prev_name + ".txt";
					delete_file(filename);
					system("cls");
					std::cout << "\ncontact renamed succsessfully\n";
					system("pause");
				}
				else {
					std::cout << "\nError! Name is bisy!\n";
					system("pause");
				}
				act_is_done = true;
			} break;

			case ESC: {
				print_pos = names.begin() + line;
				act_is_done = true;
			} break;
			}
		}
	}

	void delete_contact(int line) {
		std::string del_name = names.at(line);
		names.erase(names.begin() + line);
		if (line < names.size()) {
			print_pos = names.begin() + line;
		}
		else {
			print_pos = names.end();
			print_pos--;
		}
		findFirstLetterIntervals();
		
		delete_file(del_name + ".txt");
		std::cout << "\ncontact deleted succsessfully\n";
		system("pause");
	}

	void find_contact(std::string substr) {
		std::vector<int> interval = get_word_range_inLetterIntervals(substr);

		bool is_finded = false;
		if (!interval.empty()) {
			auto it = names.begin() + interval[0];
			for (it; it != names.begin() + interval[1] + 1 && it != names.end(); it++) {
				if (!(*it).compare(0, substr.size(), substr)) {
					print_pos = it;
					is_finded = true;
					std::cout << "\nSubstring is found\n";
					break;
				}
			}
		}
		if (!is_finded) {
			std::cout << "\nSubstring is NOT found\n";
		}
		system("pause");
	}


	void add_contact(std::string name, std::string content) {
		// new file creating
		if (std::count(names.begin(), names.end(), name) == 0) {
			std::string filename = name + ".txt";
			create_file(filename);
			write_file(filename, content);
			// printing position iterator updating
			std::vector<std::string>::iterator ins_pos = lower_bound(names.begin(), names.end(), name);
			print_pos = names.insert(ins_pos, name);
			findFirstLetterIntervals();
		}
		else {
			std::cout << "\nName is bisy!\n";
			system("pause");
		}
	}

	int printing_pos_inx() {
		return print_pos - names.begin();
	}

	void print_names() {
		// print 24 names from print_pos iterator
		set_pos(0, header_heigth);

		auto it = print_pos;
		for (int i = 0; !names.empty() && i < console_width - header_heigth - 1 && it != names.end(); i++, it++) {
			std::cout << std::left << std::setw(30) << *it << '\n';
		}
	}

	void scroll_down() {
		auto last_it = names.end();
		last_it--;

		if (print_pos != last_it) {
			print_pos++;
		}
	}

	void scroll_up() {
		if (print_pos != names.begin()) {
			print_pos--;
		}
	}

	void exit(std::string filename) {
		// exit logic
		system("cls");
		std::cout << "process is ended!\n";

		std::ofstream file(filename, std::ios_base::trunc);
		for (auto it = names.begin(); it != names.end(); it++) {
			file << (*it) << ',';
		}
		file.close();
	}

	int head_width() {
		return header_heigth;
	}

	int cons_width() {
		return console_width;
	}

	void clear_scr() {
		system("cls");
	}

	void print_main_header() {
		print_header(main_header);
	}

	unsigned int names_vec_size() {
		return names.size();
	}
};